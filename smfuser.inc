<?php

/**
 * @file
 * Com2smf functions for work with SMF's users.
 */
 
 //Clone user from Drupal to SMF
function _com2smf_user_create($account, $edit) {
  // Check the same existing user in SMF.
  if (db_num_rows(db_query("SELECT userid FROM "._smf_prefix()."user WHERE LOWER(username) = LOWER('%s')", $edit['name'])) > 0) {
    return -1;
  }
  
  $salt = _fetch_user_salt();
  // Note: Password is already hashed during user export.
  if (isset($edit['md5pass'])) {
    $passhash = md5($edit['md5pass'].$salt.'');
  }
  else {
    $passhash = md5(md5($edit['pass']).$salt.'');
  }

  $passdate = date('Y-m-d', $account->created);
  $joindate = $account->created;

  // Grabbing the user title from the database.
  $usertitle = db_result(db_query('SELECT title FROM '._smf_prefix().'usertitle WHERE minposts = 0'));

  // Grab the default timezone offset, divided by 3600 b/c the system.module timezone form is in seconds.
  $timezone = intval($timezone) / 3600;

  // Default usergroup id.
  if ($edit['status'] == 1) {
  	$usergroupid = variable_get('com2smf_default_usergroup', '2');
	}
	else {
		$usergroupid = 1;
	}

  // Create user.
  $result = db_query("INSERT INTO "._smf_prefix()."user (username, usergroupid, password, passworddate, usertitle, email, salt, showvbcode, languageid, timezoneoffset, posts, joindate, lastvisit, lastactivity) VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', 1, 0, '%s', 0, '%s', '%s', '%s')", $edit['name'], $usergroupid, $passhash, $passdate, $usertitle, $edit['mail'], $salt, $timezone, $joindate, time(), time());

  //Get ID on new user in SMF
  $userid = db_result(db_query("SELECT MAX(LAST_INSERT_ID(userid)) FROM "._smf_prefix()."user"));

  db_query("INSERT INTO "._smf_prefix()."userfield (userid) VALUES (%d)", $userid);
  db_query("INSERT INTO "._smf_prefix()."usertextfield (userid) VALUES (%d)", $userid);

  // Insert new user into cross table.
  db_query("INSERT INTO {smf_users} VALUES (%d, %d)", $account->uid, $userid);

  return $userid;
}

/**
 * Validating users' data
 */
function _com2smf_user_validate($account, &$edit, $smfuser) {
  if (db_num_rows(db_query("SELECT userid FROM "._smf_prefix()."user WHERE userid != %d AND LOWER(username) = LOWER('%s')", $smfuser, $edit['name'])) > 0) {
    form_set_error('name', t('The name %name is used in forum (%id).', array('%name' => $edit['name'], '%id' => $smfuser)));
  }
  if (db_num_rows(db_query("SELECT email FROM "._smf_prefix()."user WHERE userid != %d AND LOWER(email) = LOWER('%s')", $smfuser, $edit['mail'])) > 0) {
    form_set_error('mail', t('The E-mail %email is used in forum (%id).', array('%email' => $edit['mail'], '%id' => $smfuser)));
  }
}

/**
 * Updating user's data
 */
function _com2smf_user_update($account, &$edit, $smfuser) {
	$smfdata = db_fetch_array(db_query("SELECT userid, usergroupid, username, password, passworddate, email, parentemail, salt FROM "._smf_prefix()."user WHERE userid = %d", $smfuser));
	$updatedata = array();
	//$query = "UPDATE "._smf_prefix()."user SET ";
	//$query .= "username = '".$edit['name']."'";
	if ($edit['name'] != $smfdata['username']) $updatedata[] = "username = '" .$edit['name'] ."'";
	if ($edit['mail'] != $smfdata['email']) $updatedata[] = "email = '" .$edit['mail'] ."'";
	if ($edit['status'] == 0 && $smfdata['usergroupid'] == 2) $updatedata[] = "usergroupid = 1";
	if ($edit['status'] == 1 && $smfdata['usergroupid'] == 1) $updatedata[] = "usergroupid = 2";
	if ($edit['pass'] != '') {
		$passhash = md5(md5($edit['pass']).$smfdata['salt']);
		if ($passhash != $smfdata['password']) {
			$salt = _fetch_user_salt();
			$passhash = md5(md5($edit['pass']).$salt);
			$updatedata[] = "password = '" .$passhash ."'";
			$updatedata[] = "salt = '" .$salt ."'";
			$updatedata[] = "passworddate = '" .date('Y-m-d') ."'";
		}
	}
	//$query .= " WHERE userid = ".$smfdata['userid'];
	if (count($updatedata) > 0) {
		$query = "UPDATE " ._smf_prefix() ."user SET " .implode(', ', $updatedata) ." WHERE userid = %d";
		drupal_set_message($query, 'status');
		$result = db_query($query, $smfuser);
		return $result;
	}
}

/**
 * Loging to forum for crossed user during logging in Drupal
 */
function _com2smf_user_login($account) {
	$now = time();
	$smf_options = _smf_options();
	$smfuser = db_fetch_array(db_query("SELECT f.userid, f.password, f.salt, b.userid as banned, b.liftdate FROM {smf_users} d, " ._smf_prefix() ."user f LEFT JOIN " ._smf_prefix() ."userban b ON b.userid = f.userid WHERE d.dr_userid = %d AND f.userid = d.smf_userid", $account->uid));
	if (!$smfuser || ($smf_user['banned'] && $smf_user['liftdate'] < $now)){
		return FALSE;
	}
	
	$cookie_prefix = variable_get('com2smf_cookie_prefix', 'bb');
	$cookie_path = $smf_options['cookiepath'];
	$cookie_domain = $smf_options['cookiedomain'];
	// If cookie lifetime not set we use 1 year in seconds (60 * 60 * 24 * 365 = 31 536 000 sec)
	$cookie_expire = $now + (@ini_get('session.cookie_lifetime') ? @ini_get('session.cookie_lifetime') : 31536000);
	
	$sessionhash = md5('com2smf'.$smfuser['userid']);
	$ip = implode('.', array_slice(explode('.', _com2smf_get_idhash()), 0, 4 - $smf_options['ipcheck']));
	$idhash = md5($_SERVER['HTTP_USER_AGENT'] .$ip);
	//$cpsession = _fetch_sessionhash();

	db_query("INSERT IGNORE INTO " ._smf_prefix() ."session (sessionhash, userid, host, idhash, lastactivity, location, useragent, loggedin) VALUES ('%s', %d, '%s', '%s', %d, '%s', '%s', %d)", $sessionhash, $smfuser['userid'], $_SERVER['REMOTE_ADDR'], $idhash, $now, '/forum/index.php', $_SERVER['HTTP_USER_AGENT'], 2);
	//db_query("INSERT INTO " ._smf_prefix() ."cpsession (userid, hash, dateline) VALUES (%d, '%s', %d)", $smfuser['userid'], $cpsession, $now);
	db_query("UPDATE " ._smf_prefix() ."user SET lastvisit = %d WHERE userid = %d", $now, $smfuser['userid']);

	setcookie($cookie_prefix .'sessionhash', $sessionhash, 0, $cookie_path, $cookie_domain);
	setcookie($cookie_prefix .'lastvisit', $now, $cookie_expire, $cookie_path, $cookie_domain);
	setcookie($cookie_prefix .'lastactivity', $now, $cookie_expire, $cookie_path, $cookie_domain);
	setcookie($cookie_prefix .'userid', $smfuser['userid'], $cookie_expire, $cookie_path, $cookie_domain);
	setcookie($cookie_prefix .'password', md5($smfuser['password'] .$smfuser['salt']), $cookie_expire, $cookie_path, $cookie_domain);
	
	return TRUE;
}

/**
 * Logout from forum with exiting from site
 */
function _com2smf_user_logout($account) {
	$now = time();
	$smf_options = _smf_options();
	$smfuser = db_result(db_query("SELECT smf_userid FROM {smf_users} WHERE dr_userid = %d", $account->uid));
	if ($smfuser) {
	$cookie_prefix = variable_get('com2smf_cookie_prefix', 'bb');
	$cookie_path = $smf_options['cookiepath'];
	$cookie_domain = $smf_options['cookiedomain'];
	$cookie_expire = $now - 3600;
	
	$sessionhash = md5('com2smf'.$smfuser['userid']);
  
	db_query("DELETE FROM " ._smf_prefix() ."session WHERE sessionhash = '%s'", $sessionhash);
	db_query("UPDATE " ._smf_prefix() ."user SET lastvisit = %d WHERE userid = %d", $now, $smfuser);

	setcookie($cookie_prefix .'sessionhash', '', $cookie_expire, $cookie_path, $cookie_domain);
	setcookie($cookie_prefix .'lastvisit', '', $cookie_expire, $cookie_path, $cookie_domain);
	setcookie($cookie_prefix .'lastactivity', '', $cookie_expire, $cookie_path, $cookie_domain);
	setcookie($cookie_prefix .'userid', '', $cookie_expire, $cookie_path, $cookie_domain);
	setcookie($cookie_prefix .'password', '', $cookie_expire, $cookie_path, $cookie_domain);
	
  }
}

/**
 * SMF's system variables
 */
function _smf_options() {
	static $smf_options = NULL;
	
	if (!isset($smf_options)) {
		$result = db_query("SELECT varname, value FROM " ._smf_prefix() ."setting WHERE (grouptitle = 'http' AND varname LIKE 'cookie%') OR varname = 'ipcheck'");
		while ($smf_opt = db_fetch_array($result)) {
			$smf_options[$smf_opt['varname']] = $smf_opt['value'];
		}
	}
	
	return $smf_options;
}

/**
 * Generates a new user salt string for SMF's user
 *
 * @param	integer	(Optional) the length of the salt string to generate
 *
 * @return	string
 */
function _fetch_user_salt($length = 3) {
		$salt = '';

		for ($i = 0; $i < $length; $i++) {
			$salt .= chr(rand(33, 126));
		}

		return $salt;
	}

function _fetch_sessionhash() {
	return md5(uniqid(microtime(), true));
} 

function _com2smf_get_idhash() {
  $alt_ip = $_SERVER['REMOTE_ADDR'];

  if (isset($_SERVER['HTTP_CLIENT_IP'])) {
    $alt_ip = $_SERVER['HTTP_CLIENT_IP'];
  }
  else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && preg_match_all('#\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}#s', $_SERVER['HTTP_X_FORWARDED_FOR'], $matches)) {
    // Make sure we dont pick up an internal IP defined by RFC1918
    foreach ($matches[0] AS $ip) {
      if (!preg_match("#^(10|172\.16|192\.168)\.#", $ip)) {
        $alt_ip = $ip;
        break;
      }
    }
  }
  else if (isset($_SERVER['HTTP_FROM'])) {
    $alt_ip = $_SERVER['HTTP_FROM'];
  }
  return $alt_ip;  
}