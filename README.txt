﻿# $Id$

------------------------
COM2SMF MODULE README
------------------------
WARNING!

 This module is a variant of original COMMENT MODULE and fully replaces it for system.
 Before installing com2smf turn off and remove original COMMENT MODULE!

 This is a simple module to place comments to Drupal’s nodes in SMF's database. It provides capability to start a discussion of node in Drupal and continue it both in Drupal and SMF. The visitor of node will see full discussion. Module have some features for linking of users in both databases:
 - linking of user's accounts in Drupal and SMF;
 - automatic registration of new users from Drupal in SMF;
 - syncronasing of user's name, E-mail, password from Drupal to SMF;
 - increasing post's counter of user in SMF with comments in Drupal;
 - shared login and logoff in SMF with the same actions in Drupal;
 - shared activating/deactivating accounts from Drupal.

Also COM2SMF provides some blocks:
 - fully standart for COMMENT module block "Recent comments" with number of comments setting;
 - Last replies in forum;
 - Top posters in forum;
 - Users in forum online;
 - Hot topics.

------------------------
REQUIREMENTS
------------------------
1. This module requires Drupal and removed original COMMENT MODULE. You must remove COMMENT MODULE before starting using this module.
2. This module has limited capability with Akismet.
3. This module requires placing Drupal's and SMF's tables in same MySQL database with different prefixes.
4. The main ideology is to administer comments with the SMF. So it’s a mistake to use modules for administration comments (COMMENT MOVER MODULE and so on).
5. SMF does not allow use in posts HTML tags. So COM2SMF before storing comments replace HTML tags with bbcode tags and remove inconvertibility tags. So for normally displaying of comments from both systems you must add module for replacing bbcode tags with HTML tags (BBCODE module for example).
6. For comments posted with SMF is missing input format and for its displaying is used input format #1. You need to activate the possibilities from point above for this input format.
7. If you are using CAPTCHA to protect from spam, you need add tag to comment's form of Com2SMF: in http://example.com/admin/user/captcha/captcha/captcha_point add Form Id: com2SMF_form .

------------------------
INSTALLATION
------------------------
1. Extract the COM2SMF MODULE directory, including all its subdirectories, into Your sites/all/modules directory.
2. Turn off COMMENT MODULE and all modules that require it. You can make it on the Administer >> Site building >> Modules page.
3. Remove COMMENT MODULE from system. COMMENT MODULE usualy is placed in modules/comment directory.
3. Enable the COM2SMF MODULE on the Administer >> Site building >> Modules page.
   The database tables will be created automatically for you at this point.
4. Configure module on the Administer >> Content >> Comment >> Settings page.
5. Additional options can be set at Administer >> Content >> Comment >> Linked Users page.

------------------------
AUTHOR / MAINTAINER
------------------------
Elia Reznik <elia@reznik.kiev.ua>

------------------------
WISH LIST
------------------------
To extend a functionality of module, I need an ordered “todo list”. Help me to build this “to-do list”.
