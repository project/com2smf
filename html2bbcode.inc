<?php

/**
 * @file
 * Convert to bbcode and remove inconvertibility HTML tags from $text.
 */
 
 /**
 * Convert html code to bbocde.
 *
 * @param &$text
 *   A stirng containig the text to convert.
 * @return
 *   Converted string.
 */
 function html2bbcode($text) {
	$text = preg_replace(array(
			'#<a href="([^"]*)\[([^"]+)"(.*)>(.*)\[\\2</a>#siU', // check for the WYSIWYG editor being lame with URL tags followed by bbcode tags
			'#(<[^<>]+ (src|href))=(\'|"|)??(.*)(\\3)#esiU'  // make < and > safe in inside URL/IMG tags so they don't get stripped by strip_tags
		), array(
			'<a href="\1"\3>\4</a>[\2',                     // check for the browser (you know who you are!) being lame with URL tags followed by bbcode tags
			"_html2bbcode_clear_url('\\1', '\\4', '\\3')"                     // make < and > safe in inside URL/IMG tags so they don't get stripped by strip_tags
		), $text
	);
	// attempt to remove bad html and keep only that which we intend to parse
		$text = str_replace('<br/>', '<br />', $text);
		$text = preg_replace('#<script[^>]*>(.*)</script>#siU', '', $text);
		$text = strip_tags($text, '<b><strong><i><em><u><a><div><span><p><blockquote><ol><ul><li><font><img><br><h1><h2><h3><h4><h5><h6><small>');
	// replace &nbsp; with a regular space
		$text = str_replace('&nbsp;', ' ', $text);
		
	// regex find / replace #1
	$pregfind = array
	(
		'#<(h[0-9]+)[^>]*>(.*)</\\1>#siU',                    // headings
		'#<img[^>]+src=(\'|")(.*)(\\1).*>#esiU',              // img tag
		'#<br.*>#siU',                                        // <br> to newline
		'#<a name=[^>]*>(.*)</a>#siU',                         // kill named anchors
		'#\[(html|php)\]((?>[^\[]+?|(?R)|.))*\[/\\1\]#siUe',				// strip html from php tags
		'#\[url=(\'|"|&quot;|)<A href="(.*)/??">\\2/??</A>#siU'						// strip linked URLs from manually entered [url] tags (generic)
	);
	$pregreplace = array
	(
		"[b]\\2[/b]\n\n",                                     // headings
		"_html2bbcode_img('\\2')",                          // img tag
		"\n",                                                 // <br> to newline
		'\1',                                                  // kill named anchors
		"strip_tags_callback('\\0')",								// strip html from php tags
		'[url=$1$2'											//`strip linked URLs from manually entered [url] tags (generic)
	);
	$text = preg_replace($pregfind, $pregreplace, $text);

	// recursive code parsers
	$text = _html2bbcode_recurse('b', $text, '_html2bbcode_code_replacement', 'b');
	$text = _html2bbcode_recurse('strong', $text, '_html2bbcode_code_replacement', 'b');
	$text = _html2bbcode_recurse('small', $text, '_html2bbcode_code_replacement', 'small');
	$text = _html2bbcode_recurse('i', $text, '_html2bbcode_code_replacement', 'i');
	$text = _html2bbcode_recurse('em', $text, '_html2bbcode_code_replacement', 'i');
	$text = _html2bbcode_recurse('u', $text, '_html2bbcode_code_replacement', 'u');
	$text = _html2bbcode_recurse('a', $text, '_html2bbcode_anchor');
	$text = _html2bbcode_recurse('font', $text, '_html2bbcode_font');
	$text = _html2bbcode_recurse('blockquote', $text, '_html2bbcode_code_replacement', 'indent');
	$text = _html2bbcode_recurse('ol', $text, '_html2bbcode_list_element');
	$text = _html2bbcode_recurse('ul', $text, '_html2bbcode_list_element');
	$text = _html2bbcode_recurse('div', $text, '_html2bbcode_div');
	$text = _html2bbcode_recurse('span', $text, '_html2bbcode_span');
	
	return $text;
 }
 /**
 * Recurcive function for removing HTML tags.
 *
 * @param &$options
 *   A key of removed HTML tag
 * @param &$text
 *   A stirng containig the text to convert.
 * @param &$parceto
 *   A key of bbcode tag to replace
 * @return
 *   Converted string.
 */
function _html2bbcode_code_replacement($options, $text, $tagname, $parseto)
{
	$useoptions = array(); // array of (key) tag name; (val) option to read. If tag name isn't found, no option is used

	if (trim($text) == '')
	{
		return '';
	}

	$parseto = strtoupper($parseto);

	if (empty($useoptions["$tagname"]))
	{
		$text = _html2bbcode_recurse($tagname, $text, '_html2bbcode_code_replacement', $parseto);
		return "[$parseto]{$text}[/$parseto]";
	}
	else
	{
		$optionvalue = _html2bbcode_tag_attribute($useoptions["$tagname"], $options);
		if ($optionvalue)
		{
			return "[$parseto=$optionvalue]{$text}[/$parseto]";
		}
		else
		{
			return "[$parseto]{$text}[/$parseto]";
		}
	}
}
 /**
 * Recurcive function for removing HTML tags.
 *
 * @param &$text
 *   A stirng containig the text to convert.
 * @return
 *   Converted string.
 */
function _html2bbcode_recurse($tagname, $text, $functionhandle, $extraargs = '')
{
	$tagname = strtolower($tagname);
	$open_tag = "<$tagname";
	$open_tag_len = strlen($open_tag);
	$close_tag = "</$tagname>";
	$close_tag_len = strlen($close_tag);

	$beginsearchpos = 0;
	do {
		$textlower = strtolower($text);
		$tagbegin = @strpos($textlower, $open_tag, $beginsearchpos);
		if ($tagbegin === false)
		{
			break;
		}

		$strlen = strlen($text);

		// we've found the beginning of the tag, now extract the options
		$inquote = '';
		$found = false;
		$tagnameend = false;
		for ($optionend = $tagbegin; $optionend <= $strlen; $optionend++)
		{
			$char = $text{$optionend};
			if (($char == '"' OR $char == "'") AND $inquote == '')
			{
				$inquote = $char; // wasn't in a quote, but now we are
			}
			else if (($char == '"' OR $char == "'") AND $inquote == $char)
			{
				$inquote = ''; // left the type of quote we were in
			}
			else if ($char == '>' AND !$inquote)
			{
				$found = true;
				break; // this is what we want
			}
			else if (($char == '=' OR $char == ' ') AND !$tagnameend)
			{
				$tagnameend = $optionend;
			}
		}
		if (!$found)
		{
			break;
		}
		if (!$tagnameend)
		{
			$tagnameend = $optionend;
		}
		$offset = $optionend - ($tagbegin + $open_tag_len);
		$tagoptions = substr($text, $tagbegin + $open_tag_len, $offset);
		$acttagname = substr($textlower, $tagbegin + 1, $tagnameend - $tagbegin - 1);
		if ($acttagname != $tagname)
		{
			$beginsearchpos = $optionend;
			continue;
		}

		// now find the "end"
		$tagend = strpos($textlower, $close_tag, $optionend);
		if ($tagend === false)
		{
			break;
		}

		// if there are nested tags, this <$tagname> won't match our open tag, so we need to bump it back
		$nestedopenpos = strpos($textlower, $open_tag, $optionend);
		while ($nestedopenpos !== false AND $tagend !== false)
		{
			if ($nestedopenpos > $tagend)
			{ // the tag it found isn't actually nested -- it's past the <$tagname>
				break;
			}
			$tagend = strpos($textlower, $close_tag, $tagend + $close_tag_len);
			$nestedopenpos = strpos($textlower, $open_tag, $nestedopenpos + $open_tag_len);
		}
		if ($tagend === false)
		{
			$beginsearchpos = $optionend;
			continue;
		}

		$localbegin = $optionend + 1;
		$localtext = $functionhandle($tagoptions, substr($text, $localbegin, $tagend - $localbegin), $tagname, $extraargs);

		$text = substr_replace($text, $localtext, $tagbegin, $tagend + $close_tag_len - $tagbegin);

		// this adjusts for $localtext having more/less characters than the amount of text it's replacing
		$beginsearchpos = $tagbegin + strlen($localtext);
	} while ($tagbegin !== false);

	return $text;
}
 /**
 * Recurcive function for removing HTML tags.
 *
 * @param &$text
 *   A stirng containig the text to convert.
 * @return
 *   Converted string.
 */
function _html2bbcode_tag_attribute($option, $text)
{
	if (($position = strpos($text, $option)) !== false)
	{
		$delimiter = $position + strlen($option);
		if ($text{$delimiter} == '"')
		{ // read to another "
			$delimchar = '"';
		}
		else if ($text{$delimiter} == '\'')
		{
			$delimchar = '\'';
		}
		else
		{ // read to a space
			$delimchar = ' ';
		}
		$delimloc = strpos($text, $delimchar, $delimiter + 1);
		if ($delimloc === false)
		{
			$delimloc = strlen($text);
		}
		else if ($delimchar == '"' OR $delimchar == '\'')
		{
			// don't include the delimiters
			$delimiter++;
		}
		return trim(substr($text, $delimiter, $delimloc - $delimiter));
	}
	else
	{
		return '';
	}
}
 /**
 * Recurcive function for removing HTML tags.
 *
 * @param &$text
 *   A stirng containig the text to convert.
 * @return
 *   Converted string.
 */
function _html2bbcode_anchor($aoptions, $text)
{
	global $base_url;
	$href = _html2bbcode_tag_attribute('href=', $aoptions);

	if (!trim($href))
	{
		return _html2bbcode_recurse('a', $text, '_html2bbcode_anchor');
	}

	if (substr($href, 0, 7) == 'mailto:')
	{
		$tag = 'email';
		$href = substr($href, 7);
	}
	else
	{
		$tag = 'url';
		if (!preg_match('#^[a-z0-9]+:#i', $href))
		{
			// relative URL, prefix it with the URL to this site
			$href = $base_url.$href;
		}
	}
	$tag = strtoupper($tag);

	return "[$tag=$href]" . _html2bbcode_recurse('a', $text, '_html2bbcode_anchor') . "[/$tag]";
}
 /**
 * Recurcive function for removing HTML tags.
 *
 * @param &$text
 *   A stirng containig the text to convert.
 * @return
 *   Converted string.
 */
function _html2bbcode_font($fontoptions, $text)
{
	$tags = array(
		'font' => 'face=',
		'size' => 'size=',
		'color' => 'color='
	);
	$prependtags = '';
	$appendtags = '';

	$fontoptionlen = strlen($fontoptions);

	foreach ($tags AS $vbcode => $locate)
	{
		$optionvalue = parse_wysiwyg_tag_attribute($locate, $fontoptions);
		if ($optionvalue)
		{
			$vbcode = strtoupper($vbcode);
			$prependtags .= "[$vbcode=$optionvalue]";
			$appendtags = "[/$vbcode]$appendtags";
		}
	}

	parse_style_attribute($fontoptions, $prependtags, $appendtags);

	return $prependtags . _html2bbcode_recurse('font', $text, '_html2bbcode_font') . $appendtags;
}

function _html2bbcode_list_element($listoptions, $text)
{
	return '[*]' . rtrim($text);
}

function _html2bbcode_div($divoptions, $text)
{
	$prepend = '';
	$append = '';

	parse_style_attribute($divoptions, $prepend, $append);
	$align = parse_wysiwyg_tag_attribute('align=', $divoptions);

	// only allow left/center/right alignments
	switch ($align)
	{
		case 'left':
		case 'center':
		case 'right':
			break;
		default:
			$align = '';
	}

	$align = strtoupper($align);

	if ($align)
	{
		$prepend .= "[$align]";
		$append .= "[/$align]";
	}
	$append .= "\n";

	return $prepend . _html2bbcode_recurse('div', $text, '_html2bbcode_div') . $append;
}

function _html2bbcode_span($spanoptions, $text)
{

	$prependtags = '';
	$appendtags = '';
	parse_style_attribute($spanoptions, $prependtags, $appendtags);

	return $prependtags . _html2bbcode_recurse('span', $text, '_html2bbcode_span') . $appendtags;
}

function _html2bbcode_clear_url($type, $url, $delimiter = '\\"')
{
	static $find, $replace;
	if (!is_array($find))
	{
		$find =    array('<',    '>',    '\\"');
		$replace = array('&lt;', '&gt;', '"');
	}

	$delimiter = str_replace('\\"', '"', $delimiter);

	return str_replace('\\"', '"', $type) . '=' . $delimiter . str_replace($find, $replace, $url) . $delimiter;
}

function _html2bbcode_img($img_url)
{
	global $base_url;
	$img_url = str_replace('\\"', '"', $img_url);

	if (!preg_match('#^https?://#i', $img_url))
	{
		// relative URL, prefix it with the URL to this board
		$img_url = $base_url.$img_url;
	}

	return '[img]' . $img_url . '[/img]';
}